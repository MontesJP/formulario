// Botón que usaremos para crear el nuevo usuario
let boton = document.querySelector("#boton");
boton.addEventListener("click", crearUsuario);

// Mensaje de alerta
mensaje = document.querySelector("#mensaje");

// Modelo de clase User
class User {
    constructor(nombre, apellido, email, pais, password, repPasswrd) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.email = email;
        this.pais = pais;
        this.password = password;
        this.repPasswrd = repPasswrd;
    }
};

// Funcion para crear un nuevo usuario
function crearUsuario() {
    // Asignacion de valores a las variables
    nombre = document.querySelector("#nombre").value,
        apellido = document.querySelector("#apellido").value,
        email = document.querySelector("#email").value,
        pais = document.querySelector("#pais").value,
        password = document.querySelector("#pass1").value,
        repPasswrd = document.querySelector("#pass2").value;

    // Validación del formulario 
    // "Si ninguna casilla está vacía:..."
    if (nombre != "" && apellido != "" && email != "" && pais != "" && password != "" && repPasswrd != "") {
        // Switch off
        let existe = false;
        for (let usuario of validUsers) {
            if (usuario.email === email) {
                // si se cumple la condición ---> Switch on
                existe = true;
            }
        }
        // Verifica si el switch está "off"
        if (existe === false) {
            // Pasa a verificar la segunda condición
            if (password === repPasswrd) {
                // En caso de ambas ser ciertas, crea el usuario
                let usuario = new User(nombre, apellido, email, pais, password, repPasswrd);
                console.log(usuario);
                // Agrega la instancia creada a una lista de usuarios validos 
                validUsers.push(usuario);
                mensaje.innerHTML = "Usuario creado con exito"
            }
            else {
                mensaje.innerHTML = "El password no coincide."
            }

        }
        else {
            mensaje.innerHTML = "El mail ya esta registrado."
        }

    } else {
        mensaje.innerHTML = "Por favor complete todos los campos"
    }


};

// Acá se guardan los usuarios creados
let validUsers = [];


// LOGIN


// Botón que usaremos para intentar ingresar a nuestra cuenta
const loginButton = document.querySelector("#loginButton");
loginButton.addEventListener("click", loginValidation);

// Mensaje de alerta para el login
let loginMessage = document.querySelector("#loginMessage");



function loginValidation() {
    // Asignación de variables
    loginEmail = document.querySelector("#loginEmail").value;
    loginPassword = document.querySelector("#loginPassword").value;

    // Recorre el array donde guardamos los usuarios creados
    // y verifica que tanto el email como la contraseña coincidan en el mismo usuario.
    for (let i = 0; i < validUsers.length; i++) {
        if (validUsers[i].email === loginEmail && validUsers[i].password === loginPassword) {
            loginMessage.innerHTML = "Ingreso exitoso";
            console.log(validUsers[i]);
        } else if (validUsers[i].email != loginEmail) {
            loginMessage.innerHTML = "Email incorrecto";
            return false;
        } else if (validUsers[i].password != loginPassword) {
            loginMessage.innerHTML = "Contraseña incorrecta";
            return false;
        }
    }
}